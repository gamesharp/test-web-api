namespace TestWebApi.Model;

public class HealthCode
{
    public Guid Id { get; set; }
    public HealthyStatus Status { get; set; }
    public string PeopleId { get; set; }
}
