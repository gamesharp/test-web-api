﻿using System.ComponentModel.DataAnnotations;

namespace TestWebApi.Model.DTO
{
    public class CreateHealthyCodeModel
    {
        [Required]
        public HealthyStatus Status { get; set; }
    }
}
