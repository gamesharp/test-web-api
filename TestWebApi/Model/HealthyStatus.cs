﻿namespace TestWebApi.Model
{
    public enum HealthyStatus
    {
        Normal = 0,

        /// <summary>
        /// 已确诊
        /// </summary>
        Diagnosis = 1
    }
}
