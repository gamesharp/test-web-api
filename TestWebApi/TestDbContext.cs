using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using TestWebApi.Model;
namespace TestWebApi;

public class TestDbContext : DbContext
{
    public TestDbContext(DbContextOptions options) : base(options)
    {
    }

    public TestDbContext()
    {
    }

    public DbSet<HealthCode> HealthCodes { get; set; }
    public DbSet<Test> Tests { get; set; }
}

public class TestContextFactory : IDesignTimeDbContextFactory<TestDbContext>
{
    public TestDbContext CreateDbContext(string[] args)
    {
        var optionsBuilder = new DbContextOptionsBuilder<TestDbContext>();
        var password = Environment.GetEnvironmentVariable("CG_UserPassword");
        var username = Environment.GetEnvironmentVariable("CG_UserName");
        optionsBuilder.UseNpgsql($"server=192.168.50.177;userid={username};password={password};database={username};");

        return new TestDbContext(optionsBuilder.Options);
    }
}