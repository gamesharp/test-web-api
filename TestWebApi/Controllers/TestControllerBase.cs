﻿using FreeRedis;
using Microsoft.AspNetCore.Mvc;

namespace TestWebApi.Controllers
{
    public class TestControllerBase : ControllerBase
    {
        private RedisClient redisClient;
        private string username;

        public TestControllerBase(RedisClient redisClient)
        {
            this.redisClient = redisClient;
            username = Environment.GetEnvironmentVariable("CG_UserName");
        }

        protected string RedisGet(string key)
        {
            return redisClient.Get($"{username}:{key}");
        }

        protected void RedisSet(string key, string value)
        {
            redisClient.Set($"{username}:{key}", value);
        }
    }
}
