using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FreeRedis;
using Serilog;
using TestWebApi.Model;
using TestWebApi.Model.DTO;

namespace TestWebApi.Controllers;

[ApiController]
[Route("test")]
public class TestController : TestControllerBase
{
    private TestDbContext dbContext;

    public TestController(RedisClient redisClient, TestDbContext dbContext) : base(redisClient)
    {
        this.dbContext = dbContext;
    }

    [HttpGet]
    public string Get()
    {
        return string.Empty;
    }

    /// <summary>
    /// 更新健康码状态
    /// 在确保路由正确的前提下，课程完成后，你可以自由按需求实现这个接口
    /// </summary>
    /// <returns></returns>
    [HttpPost("/api/healthycode/{peopleId}/{status}")]
    public IActionResult UpdateHealthyCode(string peopleId, HealthyStatus status)
    {
        var healthyCode = dbContext.HealthCodes.FirstOrDefault(x => x.PeopleId == peopleId);
        if (healthyCode == null)
        {
            return NoContent();
        }

        healthyCode.Status = status;
        dbContext.SaveChanges();
        return Ok();
    }
}
